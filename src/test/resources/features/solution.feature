Feature: Test

  Background:
    Given open URL

  @ex2
  Scenario: Test_2
    When user go to page Schedule
    Then checkButtonsExist
      | Расписание          |
      | Бесплатные семинары |
      | Онлайн-курсы        |

  @ex3
  Scenario: Test_3
    When user go to page contacts
    Then check see buttons
      | Москва          |
      | Санкт-Петербург |
      | Омск            |
      | Днепр           |
      | Одесса          |
      | Киев            |
      | Одесса          |
    Then check notsee buttons
      | Екатеринбург |

  @ex4
    @searchCourse
  Scenario Outline: Test_4
    When user go to page catalog
    And search course <course>
    Then check course <linkName>
    Examples:
      | course  | linkName                                                                     |
      | SQA-050 | Школа автоматизированного тестирования. Часть 2. Selenium WebDriver          |
      | SQA-051 | Школа автоматизированного тестирования. Часть 3. BDD-тестирование с Cucumber |

