package cucumber.steps;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/*Класс нужен для запуска драйверов**/
public class Hooks {
    public static WebDriver driver;

    @Before
    public void initDriver() {
        //System.setProperty("webdriver.chrome.driver", "libs/chromedriver.exe");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

    }

    @AfterEach
    void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Before(value = "@ex2")
    public void beforeEx2(Scenario scenario) {
        System.out.println(scenario.getName());
    }

    @Before(value = "@ex3")
    public void beforeEx3(Scenario scenario) {
        System.out.println(scenario.getName());
    }

    @Before(value = "@ex4")
    public void beforeEx4(Scenario scenario) {
        System.out.println(scenario.getName());
    }

    @After(value = "@ex3", order = 5)
    public void afterEx3(Scenario scenario) {
        System.out.println(scenario.getName() + " order 5");
    }

    @After(value = "@ex3", order = 10)
    public void afterEx3again(Scenario scenario) {
        System.out.println(scenario.getName() + " order 10");
    }

    @After
    public void creteScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName());
        }
        driver.quit();
    }


}
