package cucumber.steps;

import cucumber.steps.modal.CatalogPage;
import cucumber.steps.modal.ContactsPage;
import cucumber.steps.modal.HomePage;
import cucumber.steps.modal.SchedulePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Objects;

import static cucumber.steps.modal.HomePage.Вкладки.Каталог;
import static cucumber.steps.modal.HomePage.Вкладки.Расписание;

public class SolutionSteps {
    private HomePage homePage = new HomePage();
    private SchedulePage schedulePage = new SchedulePage();
    private ContactsPage contactsPage = new ContactsPage();
    private CatalogPage catalogPage = new CatalogPage();

    private String baseURL = "https://www.luxoft-training.ru/";

    @When("user go to page Schedule") //Пользователь переходит на вкладку Расписание
    public void userGoToPageSchedule() {
        homePage.userGoToPage(Расписание);
    }

    @When("user go to page contacts") //Пользователь переходит на вкладку Контакты
    public void userGoToPageContacts() {
        homePage.userGoToPageContacts();
    }

    @When("user go to page catalog") //Пользователь переходит на вкладку Каталог
    public void userGoToPageCatalog() {
        homePage.userGoToPage(Каталог);
    }

    @Given("open URL")
    public void openURL() {
        Hooks.driver.manage().window().maximize();
        Hooks.driver.get(baseURL);
    }

    @Then("checkButtonsExist") //Проверка кнопок
    public void check_buttons_exist(List<String> buttons) {
        for (String button : buttons) {
            schedulePage.check_buttons_exist(button);
        }
    }

    @Then("^check (see|notsee) buttons$") //Проверка видимых и не видимых городов
    public void checkСityButtons(String shows, List<String> cities) {
        if (Objects.equals(shows, "see"))
            for (String city : cities) {
                contactsPage.checkThatCityButtonByCityName(city);
            }
        if (Objects.equals(shows, "notsee"))
            for (String city : cities) {
                contactsPage.checkThatCityButtonByCityNameNotSee(city);
            }
    }

    @When("search course {word}") //на странице представлена ссылка с названием курса
    public void searchCourseCourse(String course) {
            catalogPage.searchCourse(course);
    }

    @Then("check course {}") //на странице представлена ссылка с названием курса
    public void checkCourseCourseName(String linkName) {
        catalogPage.checkCourse(linkName);
    }

}
