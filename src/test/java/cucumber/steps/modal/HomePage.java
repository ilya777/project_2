package cucumber.steps.modal;


import cucumber.steps.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Класс описывает страницу 'HomePage'
 */
public class HomePage {

    public enum Вкладки {
        Каталог("Каталог")
        , Расписание("Расписание")
        , Корпоративное_обучение("Корпоративное обучение")
        , Оценка_персонала("Оценка персонала")
        , Консалтинг("Консалтинг")
        , IT_гуру("IT-гуру");

        private String имя_вкладки;

        Вкладки(String name) {
            имя_вкладки = name;
        }
    }

    /**Данный универсальный метод позволяет нажимать наразличные вкладки*/
    public void userGoToPage(Вкладки имя_вкладки) {
        WebElement требуемая_вкладка = Hooks.driver.findElement(By.xpath("//a[text()='" + имя_вкладки + "']"));
        требуемая_вкладка.click();
    }

    /**Данный метод переходит на страницу "Контакты"*/
    public void userGoToPageContacts() {
        WebElement праваяВверхняяПанель = Hooks.driver.findElement(By.xpath("//div[@class='header__control _nav']"));
        праваяВверхняяПанель.click();
        WebElement контакты = Hooks.driver.findElement(By.xpath("//li//a[text()='Контакты']"));
        контакты.click();
    }


}
