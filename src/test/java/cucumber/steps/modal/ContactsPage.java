package cucumber.steps.modal;

import cucumber.steps.Hooks;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Класс описывает страницу 'Контакты'
 */
public class ContactsPage {

    /**
     * Данный метод проверяет города, которые есть на странице
     */
    public void checkThatCityButtonByCityName(String city) {
        WebElement кнопка = Hooks.driver.findElement(By.xpath("//ul[@class='addition-menu']//a[text()='" + city + "']"));
        Assertions.assertEquals(city, кнопка.getText(),
                "Элемент '" + city + "' не найден на странице");
    }

    /**
     * Данный метод проверяет города, которых нет на странице
     */
    public void checkThatCityButtonByCityNameNotSee(String city) {
        WebElement кнопка = Hooks.driver.findElement(By.xpath("//ul[@class='addition-menu']//a[text()='" + city + "']"));
        Assertions.assertFalse(кнопка.isEnabled(), "Элемент '" + city + "' найден на странице");
    }
}
