package cucumber.steps.modal;

import cucumber.steps.Hooks;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Класс описывает страницу 'Каталог курсов по направлениям'
 */
public class CatalogPage {

    /**
     * Метод предназначен для ввода курсв в поисковую строку и его дальнейший выбор
     */
    public void searchCourse(String course) {
        WebElement полеВвода = Hooks.driver.findElement(By.xpath("//div[@class='search container']//input"));
        полеВвода.sendKeys(course);
        Hooks.driver.findElements(By.xpath("//div[@class='search container']//input")).get(1).click();
    }

    /**
     * Метод для проверки отображения курсов на странице
     */
    public void checkCourse(String course) {
        Assertions.assertTrue(Hooks.driver.findElement(By.xpath("//div[@class='search-item']"))
                .getText().contains(course), "Не правильный курс");
    }

}
