package cucumber.steps.modal;

import cucumber.steps.Hooks;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Класс описывает страницу 'Расписание и цены'
 */
public class SchedulePage {

    /**Данный универсальный метод позволяет проверять кнопки на странице*/
    public void check_buttons_exist(String button) {
        WebElement кнопка = Hooks.driver.findElement(By.xpath("//ul[@class='addition-menu']//a[text()='" + button + "']"));
        Assertions.assertEquals(button, кнопка.getText(),
                "Кнопка: '" + button + "' отсутсвует на странице");

    }
}
