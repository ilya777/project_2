package cucumber.pratice;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        //features = {"src/test/resources/features"},
        features = {"classpath:moreFeatures"},
        glue = {"cucumber.steps"},
        dryRun = false,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        //plugin = {"pretty", "html:target/results.html"}
        //tags = "@second"
        plugin = {
        "json:target/cucumber3.json",
        "pretty",
        "html:target/cucumber-reports/cucumber-pretty.html",
}


)
public class Test {
}
